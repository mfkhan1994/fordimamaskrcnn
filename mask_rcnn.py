import os
import sys
import random
import math
import numpy as np
import skimage.io
import matplotlib
import matplotlib.pyplot as plt
import cv2
import time

# Root directory of the project
ROOT_DIR = os.path.abspath("./")

# Import Mask RCNN
sys.path.append(ROOT_DIR)  # To find local version of the library
from mrcnn import utils
import mrcnn.model as modellib
from mrcnn import visualize
# Import COCO config
sys.path.append(os.path.join(ROOT_DIR, "samples/coco/"))  # To find local version
import coco


# Directory to save logs and trained model
MODEL_DIR = os.path.join(ROOT_DIR, "logs")

# Local path to trained weights file
COCO_MODEL_PATH = os.path.join(ROOT_DIR, "mask_rcnn_coco.h5")
# Download COCO trained weights from Releases if needed
if not os.path.exists(COCO_MODEL_PATH):
    utils.download_trained_weights(COCO_MODEL_PATH)

# Directory of images to run detection on
IMAGE_DIR = os.path.join(ROOT_DIR, "images")


class InferenceConfig(coco.CocoConfig):
    # Set batch size to 1 since we'll be running inference on
    # one image at a time. Batch size = GPU_COUNT * IMAGES_PER_GPU
    GPU_COUNT = 1
    IMAGES_PER_GPU = 1

config = InferenceConfig()
config.display()


# Create model object in inference mode.
model = modellib.MaskRCNN(mode="inference", model_dir=MODEL_DIR, config=config)

# Load weights trained on MS-COCO
model.load_weights(COCO_MODEL_PATH, by_name=True)

# COCO Class names
# Index of the class in the list is its ID. For example, to get ID of
# the teddy bear class, use: class_names.index('teddy bear')
class_names = ['BG', 'person', 'bicycle', 'car', 'motorcycle', 'airplane',
               'bus', 'train', 'truck', 'boat', 'traffic light',
               'fire hydrant', 'stop sign', 'parking meter', 'bench', 'bird',
               'cat', 'dog', 'horse', 'sheep', 'cow', 'elephant', 'bear',
               'zebra', 'giraffe', 'backpack', 'umbrella', 'handbag', 'tie',
               'suitcase', 'frisbee', 'skis', 'snowboard', 'sports ball',
               'kite', 'baseball bat', 'baseball glove', 'skateboard',
               'surfboard', 'tennis racket', 'bottle', 'wine glass', 'cup',
               'fork', 'knife', 'spoon', 'bowl', 'banana', 'apple',
               'sandwich', 'orange', 'broccoli', 'carrot', 'hot dog', 'pizza',
               'donut', 'cake', 'chair', 'couch', 'potted plant', 'bed',
               'dining table', 'toilet', 'tv', 'laptop', 'mouse', 'remote',
               'keyboard', 'cell phone', 'microwave', 'oven', 'toaster',
               'sink', 'refrigerator', 'book', 'clock', 'vase', 'scissors',
               'teddy bear', 'hair drier', 'toothbrush']
# class_names = ['BG', 'person', 'bicycle', 'car', 'motorcycle', 'airplane']

# Load a random image from the images folder
# file_names = next(os.walk(IMAGE_DIR))[2]
# image = skimage.io.imread(os.path.join(IMAGE_DIR, random.choice(file_names)))

def filter_class_ids(r):

    rr = dict()
    rr['class_ids'] = np.empty((0,),np.int32)
    rr['scores'] = np.empty((0,))
    rr['rois'] = np.empty((0,r['rois'].shape[1]))
    rr['masks'] = np.empty((r['masks'].shape[0], r['masks'].shape[1], 0))

    for idx, id in enumerate(r['class_ids']):
        if class_names[id] in ['person', 'car']:
            rr['class_ids'] = np.hstack((rr['class_ids'], id))
            rr['scores'] = np.hstack((rr['scores'],r['scores'][idx]))
            rr['rois'] = np.vstack((rr['rois'], r['rois'][idx, :]))
            rr['masks'] = np.append(rr['masks'], np.expand_dims(r['masks'][:, :, idx],axis=-1),axis=-1)
    return rr

#def masked_frame(image):

if __name__ == "__main__":

    scale_down = 2
    show_video = True
    show_fps = False
    save_video = False
    save_video_file = 'output.avi'
    forward_video_in_secs = 60 * 5
    skip_frames = 25
    process_on_batch = 1

    sample_video = 'samples/gm/Kiosk_2019-04-09_19-00-01.mp4'

    video_capture = cv2.VideoCapture(sample_video)

    video_fps = video_capture.get(cv2.CAP_PROP_FPS)

    video_capture.set(cv2.CAP_PROP_POS_MSEC, video_fps * forward_video_in_secs)


    video_width = int(video_capture.get(3) // scale_down)
    video_height = int(video_capture.get(4) // scale_down)

    print(video_width, video_height)

    if save_video:
        out = cv2.VideoWriter(save_video_file, 0x7634706d, 5.0, ( video_width, video_height))

    fps_algo = 0
    fps_whole = 0

    while True:

        start_time = time.time()

        batch_frames = []

        for j in range(process_on_batch):
            ret, frame = video_capture.read()
            for i in range(skip_frames):
                ret, frame = video_capture.read()

            frame = cv2.resize(frame,(video_width, video_height))
            res_frame = frame

            batch_frames.append(frame)


        mid_time = time.time()

        # Run detection
        results = model.detect(batch_frames, verbose=0)
        results = [filter_class_ids(res) for res in results]

        # print(type(results))
        # print(results)
        # exit(0)

        # Visualize results

        # Visualization starts here


        fps_algo = (fps_algo + 1/(time.time() - start_time))/2
        fps_whole = (fps_whole + 1/(time.time() - mid_time))/2

        #for res, frame in zip(results, batch_frames):

        res = results[0]

        res_frame = visualize.display_instances(frame, res['rois'], res['masks'], res['class_ids'],
                    class_names, res['scores'])
        if show_fps:
            cv2.putText(res_frame,'FPS: {0:.2f} - {0:.2f}'.format(fps_algo,fps_whole),(10,40), cv2.FONT_HERSHEY_SIMPLEX, 1,(0,150,255),2,cv2.LINE_AA)

            print('processing next batch')
        if save_video:
            out.write(res_frame)

        if show_video:
            cv2.imshow('image',res_frame)
            if cv2.waitKey(1) & 0xFF == ord('q'):
               break


